const { MODULESPECIFIER_TYPES } = require("@babel/types");

const defangIPaddr = function (address) {
  return address.replace(/\./g, "[.]");
};

module.exports = { defangIPaddr };
