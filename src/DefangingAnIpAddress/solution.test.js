const { defangIPaddr } = require("./solution");

describe("Defanging An Ip Address", () => {
  const testCases = [
    {
      Input: "1.1.1.1",
      Output: "1[.]1[.]1[.]1",
    },
    {
      Input: "255.100.50.0",
      Output: "255[.]100[.]50[.]0",
    },
  ];

  testCases.forEach((option) => {
    test(`should return ${option.Output} for ${option.Input} `, () => {
      expect(defangIPaddr(option.Input)).toStrictEqual(option.Output);
    });
  });
});
