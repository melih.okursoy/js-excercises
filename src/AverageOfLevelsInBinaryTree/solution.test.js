const { root, LevelAvg } = require("./solution");

describe("Binary Tree Avarage of Levels Code", () => {
  beforeEach(() => {});
  test("should return [] for null", () => {
    expect(LevelAvg(null)).toStrictEqual([]);
  });

  test("should return [3,14.5,11] for root", () => {
    expect(LevelAvg(root)).toStrictEqual([3, 14.5, 11]);
  });

  test("should return [3,14.5,10.5] when we reduceby 1 on 3rd level", () => {
    --root.right.right.value;
    expect(LevelAvg(root)).toStrictEqual([3, 14.5, 10.5]);
  });
});
