const LevelAvg = (root) => {
  // if (!(root instanceof Node)) {
  //   throw TypeError("invalid root parameter");
  // }

  let levelData = [];
  const traverse = (node, level) => {
    if (!node) {
      return;
    }
    if (!levelData[level]) {
      levelData[level] = [{ count: 1, sum: node.value }];
    } else {
      levelData[level] = levelData[level].map((cur) => ({
        count: cur.count + 1,
        sum: cur.sum + node.value,
      }));
    }

    traverse(node.left, level + 1);
    traverse(node.right, level + 1);
  };

  traverse(root, 0);

  return levelData.map((cur) => cur[0].sum / cur[0].count);
};

class Node {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

const root = new Node(3);
root.left = new Node(9);
root.right = new Node(20);
root.right.left = new Node(15);
root.right.right = new Node(7);

//console.log(LevelAvg(root));

module.exports = { Node, root, LevelAvg };
