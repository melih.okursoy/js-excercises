const map = {};

/**
 * Encodes a URL to a shortened URL.
 *
 * @param {string} longUrl
 * @return {string}
 */
const encode = (longUrl) => {
  let ret;

  do {
    ret = Math.random().toString(32).slice(5); // 5 char random string
  } while (map[ret]); // make sure no collisions

  map[ret] = longUrl;

  return "http://tinyurl.com/" + ret;
};

/**
 * Decodes a shortened URL to its original URL.
 *
 * @param {string} shortUrl
 * @return {string}
 */
const decode = (shortUrl) => {
    return map[shortUrl.split('com/')[1]];
};

/**
 * Your functions will be called as such:
 * decode(encode(url));
 */

module.exports = { encode, decode };
