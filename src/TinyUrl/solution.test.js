const { encode, decode } = require("./solution");

const testcases = ["www.google.com", "www.facebook.com", "www.bing.com"];
const shorturls = [];
testcases.forEach((element) => {
  shorturls.push(encode(element));
});

describe("Tiny Url", () => {
  testcases.forEach((element, index) => {
    test(`should return ${element} for ${shorturls[index]}`, () => {
      expect(decode(shorturls[index])).toStrictEqual(testcases[index]);
    });
  });
});
