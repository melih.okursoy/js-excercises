/**
 * @param {number[]} nums
 * @return {number[]}
 */
const runningSum = (nums) => {
  return nums.reduce((accumulator, current, index) => {
    accumulator.push((accumulator[index - 1] || 0) + current);
    return accumulator;
  }, []);
};

module.exports = { runningSum };
