const { runningSum } = require("./solution");

describe("Running Sum Of 1 D Array", () => {
  test("should return [1,2,3,4,5] for [1,1,1,1,1]", () => {
    expect(runningSum([1, 1, 1, 1, 1])).toStrictEqual([1, 2, 3, 4, 5]);
  });
});
